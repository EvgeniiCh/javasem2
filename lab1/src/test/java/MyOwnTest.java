import FS.Directory;
import FS.FMain;
import FS.MyOwn;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("My Own node")
public class MyOwnTest {
    @Test
    @DisplayName("Create my own node")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class, () -> MyOwn.create("test", null));

        var dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> MyOwn.create("", dir));

        var file = MyOwn.create("file sample", dir);
        assertNotNull(file);
        assertEquals(FMain.FMainType.MY_OWN, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read write")
    void readWriteTest() {
        var dir = Directory.create("dir", null);
        var file = MyOwn.create("file", dir);

        file.write("some data ");
        assertEquals("some data ", file.read());
        assertThrows(IllegalArgumentException.class, () -> file.write(null));
    }
}